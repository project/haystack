<?php
/**
 * @file
 * Settings form and related functionality for the Haystack module.
 */

/**
 * Configuration Form.
 */
function haystack_settings($form) {
  $form = array();

  $health = _haystack_health_check();

  //Add JS
   $api_key = variable_get('haystack_api_key', '');

  $form['original_api_key'] = array(
    '#type'  => 'value',
    '#value' => $api_key,
  );

  //API Key
  $form['api'] = array(
    '#type'        => 'fieldset',
    '#tree'        => FALSE,
    '#title'       => t('Server Settings'),
    '#collapsible' => TRUE,
    '#collapsed'   => empty($api_key) ? FALSE : TRUE,
    '#description' => t('Please enter your API key.
      To get an API key, register at <a href="@register_link" target="_blank">Haystack</a>; after logging-in, click "Add a New Site".', 
      array(
        '@register_link' => 'https://haystack.menu/register',
        )
      ),
  );

  $form['api']['haystack_api_key'] = array(
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#title'         => t('API Key'),
    '#default_value' => $api_key,
  );

  if ($health == 0) { //init
    $form['actions']['save_config'] = array(
      '#type'          => 'submit',
      '#title'         => t('Save Configuration'),
      '#default_value' => t('Save Configuration'),
    );

  }
  else { //ready to init

    $form['api']['haystack_read_only'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Read-Only Mode'),
      '#description'   => t('If enabled no data will be send to the Haystack server.'),
      '#default_value' => variable_get('haystack_read_only', FALSE),
    );


    $search_id_key              = variable_get('haystack_search_id_key', '');
    $haystack_quick_links       = variable_get('haystack_quick_links', '');
    $haystack_quick_links_title = variable_get('haystack_quick_links_title',
      'Quick Links');
    $haystack_status            = haystack_search_status();



    $cron_ago = _get_timeago(variable_get('cron_last'));

    if ($health > 1) {
      //Search Update Status
      $form['status'] = array(
        '#type'        => 'fieldset',
        '#tree'        => FALSE,
        '#title'       => t('Search Indexer Status'),
        '#collapsible' => TRUE,
        '#collapsed'   => FALSE,
      );

      $process_total = variable_get('process_total',0);
      $process_pos = variable_get('process_pos',0);
      $total = _haystack_total_items();

      $form['status']['#description'] = t('<p>Your Haystack index contains '.$total.' items.');
      if ($process_total > 0) {
        $form['status']['#description'] .= t(' There are '.($process_total-$process_pos).' items left in the queue. Please note that the queue may be larger than the total of items depending on when you last ran <a href="/admin/config/system/cron">Cron</a>.');
      }
      $form['status']['#description'] .= t('</p>');
    }

    //Id for Search Field
    $form['searchId'] = array(
      '#type'        => 'fieldset',
      '#tree'        => FALSE,
      '#title'       => t('Search ID Field'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
      '#description' => t('The ID of the search field on your site (e.g. #search_id). If left blank the Haystack footer will be added to the website.'),
    );

    $form['searchId']['haystack_search_id_key'] = array(
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => t('Search ID Field'),
      '#default_value' => $search_id_key,
    );

    //Links
    $form['quick_links'] = array(
      '#type'        => 'fieldset',
      '#tree'        => FALSE,
      '#title'       => t('Quick Links'),
      '#collapsible' => TRUE,
      '#collapsed'   => empty($haystack_quick_links) || empty($haystack_quick_links_title) ? FALSE : TRUE,
      '#description' => t('Please enter the top links and titles for your website. These will appear as the top links when Haystack is launched. If left empty the results will show up blank.'),
    );

    $form['quick_links']['haystack_quick_links_title'] = array(
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => t('Title for the quick links field.'),
      '#default_value' => $haystack_quick_links_title,
    );

    $form['quick_links']['haystack_quick_links'] = array(
      '#type'          => 'textarea',
      '#required'      => FALSE,
      '#title'         => t('<p>The top pages and links for your website in &lt;li&gt;&lt;a href="LINK"&gt;TITLE&lt;/a&gt;&lt;/li&gt; format.</p><p>The recommended number of list items is 5.</p>'),
      '#default_value' => $haystack_quick_links,
    );

    //Content Types
    $form['ct'] = array(
      '#type'        => 'fieldset',
      '#tree'        => FALSE,
      '#title'       => t('Content Types'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    );

    //Types
    $types = _haystack_get_content_types();
    if ($health == 1) {
      $types_value = array_keys($types); //All
    }
    else {
      $types_value = variable_get('haystack_content_types', array());
    }
    $form['ct']['haystack_content_types'] = array(
      '#type'          => 'checkboxes',
      '#required'      => FALSE,
      '#title'         => t('Include the following content types in the search index:'),
      '#default_value' => $types_value,
      '#options'       => $types,
    );

    $form['menu'] = array(
      '#type'        => 'fieldset',
      '#tree'        => FALSE,
      '#title'       => t('Menus'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    );

    //Menus
    $menus = _haystack_get_menus();
    if ($health == 1) {
      $menus_value = array_keys($menus); //All
    }
    else {
      $menus_value = variable_get('haystack_menus', array());
    }
    $form['menu']['haystack_menus'] = array(
      '#type'          => 'checkboxes',
      '#required'      => FALSE,
      '#title'         => t('Include the following menus in the search index:'),
      '#default_value' => $menus_value,
      '#options'       => $menus,
    );

    $form['actions']['save_config'] = array(
      '#type'          => 'submit',
      '#title'         => t('Save Configuration'),
      '#default_value' => t('Save Configuration'),
    );

    if ($health > 1) {
      $index_title                 = 'Re-Index Website';
      $form['actions']['re_index'] = array(
        '#type'          => 'submit',
        '#title'         => t($index_title),
        '#default_value' => t($index_title),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Extra validation for the settings form.
 *
 * @param $form
 * @param $form_state
 */
function haystack_settings_validate($form, $form_state) {
  if ($form_state['values']['op'] == 'Save Configuration') {
    _haystack_update_config($form, $form_state);
    // drupal_set_message('Haystack save config: '.$form_state['values']['op']);
  }
  elseif ($form_state['values']['op'] == 'Re-Index Website') {
    _haystack_submit_reindex($form, $form_state);
  }
}

function _haystack_update_config($form, $form_state) {
  $ori_key = $form_state['values']['original_api_key'];
  $new_key = $form_state['values']['haystack_api_key'];
  $health  = _haystack_health_check();

  // We clear out the client link and token right away if there is something off with the API key.
  if (empty($ori_key) || $ori_key != $new_key) {
    variable_set('haystack_client_hash', '');

    $success = _haystack_get_credentials($form_state['values']['haystack_api_key']);
    if (!$success) {
      form_set_error('haystack_api_key',
        t('The API Key you are using is not valid.'));
      return;
    }

    //Else reprocess all content and send to the new index
    $package = array(
      'api_token' => $new_key,
    );
    _haystack_api_call($package, 'index', 'delete_all');
    variable_set('haystack_content_types', array());
    variable_set('haystack_menus', array());
  }

  if ($health == 1) {
    variable_set('haystack_first_index', TRUE);
  }
  if ($health > 0) {
    _haystack_set_process_content_and_menus($form, $form_state);
  }
}

function _haystack_submit_reindex($form, $form_state) {
  drupal_set_message(t('Haystack has re-indexed'));
  $new_key = $form_state['values']['haystack_api_key'];

  //Delete all on server
  $package = array(
    'api_token' => $new_key,
  );
  _haystack_api_call($package, 'index', 'delete_all');

  _haystack_reset_meter_total();

  //Delete set items, we will use ones set in form
  variable_set('haystack_content_types', array());
  variable_set('haystack_menus', array());

  //Process new contents against empty initial results
  _haystack_set_process_content_and_menus($form, $form_state);
}

function _haystack_set_process_content_and_menus($form, $form_state) {
  $new_key = $form_state['values']['haystack_api_key'];

  //Check if content types have changed...
  $c_name = 'content types';
  $m_name = 'menus';

  $original_types = array(
    $c_name => variable_get('haystack_content_types',array()),
    $m_name => variable_get('haystack_menus',array()),
  );
  $types          = array(
    $c_name => $form_state['values']['haystack_content_types'],
    $m_name => $form_state['values']['haystack_menus'],
  );

  $diff_add = array($c_name => array(), $m_name => array());
  $diff_sub = array($c_name => array(), $m_name => array());



  foreach ($types as $key => $val) { //Checks the difference and adds them, subrtract or add
    foreach ($val as $k => $v) {
      $ori = isset($original_types[$key][$k]) && $original_types[$key][$k] != '0';
      $new = $v != '0';

      if ($new && !$ori) {
        $diff_add[$key][$k] = $v;
      }
      elseif ($ori && !$new) {
        $diff_sub[$key][$k] = $v;
      }
    }
  }

  $package = array(
    'api_token' => $new_key,
  );


  //Delete Array
  foreach ($diff_sub as $key => $val) { //We use keys as the val may be set 0
    if (!empty($val)) {
      drupal_set_message(t('Haystack has removed the following @key: @keys',
        array('@key' => $key, '@keys' => implode(', ', array_keys($val)))
      ));

      //Delete by Post Type
      if ($key == $c_name) { //Content
        foreach ($val as $k => $v) { //Delete each type individually
          _haystack_api_call($package, $k, 'delete_type');
        }
      }

      //Delete by Menu Name
      if ($key == $m_name) {
        _haystack_menu_indexer(array_keys($val), TRUE); //Delete is second param
      }
    }
  }

  //Add Array
  $tmp_total = 0;
  foreach ($diff_add as $key => $val) { //We use keys as the val may be set 0
    if (!empty($val)) {
      drupal_set_message(t('Haystack has added the following @key: @keys',
        array('@key' => $key, '@keys' => implode(', ', array_keys($val)))
      ));

      //Update by Post Type
      if ($key == $c_name) {
        foreach ($val as $k => $v) {
          $nids = _haystack_get_nodes($k);
          $tmp_total += count($nids);
          foreach ($nids as $nid) {
            search_touch_node($nid); //Update timestamp forcing reindex
          }
        }
      }

      //Update by Menu Name
      elseif ($key == $m_name) {
        _haystack_menu_indexer(array_keys($val)); //Reindex menu
      }

    }
  }
  if ($tmp_total > 0) {
    _haystack_add_meter_total($tmp_total);
  }
}

/**
 * Creates a list of all available Content Types on the site.
 */
function _haystack_get_content_types() {
  $types = node_type_get_types();

  $list = array();

  foreach ($types as $type) {
    $list[$type->type] = $type->name;
  }
  ksort($list);

  return $list;
}


/**
 * Creates a list of all available menus on the site.
 */
function _haystack_get_menus() {
  $result = db_query("SELECT * FROM {menu_custom} ORDER BY title", array(),
    array('fetch' => PDO::FETCH_ASSOC));

  $list = array();

  foreach ($result as $menu) {
    $list[$menu['menu_name']] = $menu['title'];
  }

  ksort($list);

  return $list;
}

function _get_timeago($ptime) {
  $estimate_time = time() - $ptime;

  if ($estimate_time < 1) {
    return 'less than 1 second ago';
  }

  $condition = array(
    12 * 30 * 24 * 60 * 60 => 'year',
    30 * 24 * 60 * 60      => 'month',
    24 * 60 * 60           => 'day',
    60 * 60                => 'hour',
    60                     => 'minute',
    1                      => 'second'
  );

  foreach ($condition as $secs => $str) {
    $d = $estimate_time / $secs;

    if ($d >= 1) {
      // $re = loop for remainder
      $r = round($d);
      return 'about ' . $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
    }
  }
}